#### Mailing service

## Deploy

1. Set environment variables:
   - cp .env.example .env and type the PROBE_SERVER_TOKEN in it 
2. Create and start db and redis containers:
   - docker-compose up -d redis db
3. Create and start web, celery and flower containers:
   - docker-compose up -d web celery_worker celery_beat flower


## App

1. http://localhost:8000 - username: admin, password: password
2. http://localhost:8000/docs/ - API documentation
3. http://localhost:5555/ - flower