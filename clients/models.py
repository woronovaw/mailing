import uuid

from django.core.validators import (
    RegexValidator,
    MinValueValidator,
    MaxValueValidator,
    MinLengthValidator,
)
from django.db import models
from django.db import models
from django.utils.translation import gettext_lazy as _

from auditlog.registry import auditlog

from .timezones import TIMEZONES


class OperatorCode(models.Model):
    code = models.PositiveIntegerField(
        _("code"),
        unique=True,
        validators=[
            MinValueValidator(limit_value=100),
            MaxValueValidator(limit_value=999),
        ],
    )

    def __str__(self):
        return str(self.code)


class Tag(models.Model):
    tag = models.CharField(_("tag"), max_length=255, unique=True)

    def __str__(self):
        return self.tag


class Client(models.Model):
    TIMEZONE_CHOICES = tuple(zip(TIMEZONES, TIMEZONES))

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    phone = models.CharField(
        _("phone number"),
        max_length=11,
        unique=True,
        validators=[
            MinLengthValidator(limit_value=11),
            RegexValidator(
                r"7\d{10}",
                message=_(
                    'The phone number must be in the format "7XXXXXXXXXX"'
                ),
            ),
        ],
    )
    code = models.ForeignKey(
        OperatorCode,
        verbose_name=_("code"),
        on_delete=models.PROTECT,
        related_name="+",
    )
    tag = models.ForeignKey(
        Tag,
        verbose_name=_("tag"),
        on_delete=models.SET_NULL,
        null=True,
        related_name="+",
    )
    timezone = models.CharField(
        _("time zone"),
        max_length=255,
        choices=TIMEZONE_CHOICES,
        default="Europe/Moscow",
    )

    def __str__(self):
        return self.phone


auditlog.register(Client)
