from django.contrib import admin

from .models import OperatorCode, Tag, Client


class ClientAdmin(admin.ModelAdmin):
    list_display = ("id", "phone", "code", "tag", "timezone")


admin.site.register(OperatorCode)
admin.site.register(Tag)
admin.site.register(Client, ClientAdmin)
