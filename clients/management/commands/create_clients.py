from django.core.management.base import BaseCommand

from django.core.management import call_command


class Command(BaseCommand):
    help = "Create test clients"

    def handle(self, *args, **options):
        call_command("loaddata", "init_clients.json")
