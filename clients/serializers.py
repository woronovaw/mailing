from rest_framework import serializers

from .models import Client, OperatorCode, Tag


class ClientSerializer(serializers.ModelSerializer):
    code = serializers.CharField(
        max_length=3,
        min_length=3,
        validators=[],
    )
    tag = serializers.CharField(max_length=255)

    class Meta:
        model = Client
        fields = fields = ("id", "phone", "code", "tag", "timezone")
        read_only_fields = ("id",)

    def create(self, validated_data):
        code = validated_data.pop("code")
        tag = validated_data.pop("tag")
        code, _ = OperatorCode.objects.get_or_create(code=code)
        tag, _ = Tag.objects.get_or_create(tag=tag)
        return self.Meta.model.objects.create(
            **validated_data, code=code, tag=tag
        )

    def update(self, instance, validated_data):
        code = validated_data.get("code")
        tag = validated_data.get("tag")

        if code:
            code, _ = OperatorCode.objects.get_or_create(code=code)
        if tag:
            tag, _ = Tag.objects.get_or_create(tag=tag)

        instance.code = code or instance.code
        instance.tag = tag or instance.tag
        instance.phone = validated_data.get("phone", instance.phone)
        instance.timezone = validated_data.get("timezone", instance.timezone)
        instance.save()
        return instance
