from django.db.models import Sum, Case, Count, When, Q
from django.db.models.functions import Coalesce

from rest_framework import viewsets
from rest_framework import mixins

from .models import Mailing, Message
from .serializers import (
    MailingSerializer,
    MessageSerializer,
    MailingStatiscticSerializer,
)


class MailingViewSet(viewsets.ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()


class MailingStatisticViewSet(
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    serializer_class = MailingStatiscticSerializer
    queryset = Mailing.objects.prefetch_related("messages")

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .annotate(
                clients=Count("messages__client"),
                draft=Count(
                    "messages", filter=Q(messages__status=Message.DRAFT)
                ),
                sending=Count(
                    "messages", filter=Q(messages__status=Message.SENDING)
                ),
                sent=Count(
                    "messages", filter=Q(messages__status=Message.SENT)
                ),
                overdue=Count(
                    "messages", filter=Q(messages__status=Message.OVERDUE)
                ),
                delete=Count(
                    "messages", filter=Q(messages__status=Message.DELETED)
                ),
            )
        )
