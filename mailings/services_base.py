import logging
from abc import ABC, abstractmethod

import requests
from requests.exceptions import RequestException


logger = logging.getLogger(__name__)


class AbstractClient(ABC):
    url = None
    authorization = None
    extra_headers = None
    timeout = None

    def __init__(
        self, url, authorization=None, extra_headers=None, timeout=None
    ):
        self.url = url
        self.authorization = authorization
        self.extra_headers = extra_headers
        self.timeout = timeout
        self.session = requests.Session()

    def __str__(self):
        return f"<{self.__class__.__name__} {self.url}>"

    def __repr__(self):
        return self.__str__()

    @property
    @abstractmethod
    def service_class(self):
        pass

    @property
    @abstractmethod
    def resource_class(self):
        pass

    @property
    @abstractmethod
    def fields(self):
        pass

    @property
    def service(self):
        return self.service_class(self, self.resource_class, self.fields)

    def execute(self, method="post", **kwargs):
        return self._do_request(method, **kwargs)

    def _build_request_headers(self):
        headers = {"Accept": "application/json"}

        if self.authorization:
            headers["Authorization"] = self.authorization

        if self.extra_headers is not None:
            headers = {**headers, **self.extra_headers}

        return headers

    def _build_request_url(self, id):
        url = self.url.rstrip("/")
        if id is not None:
            return f"{url}/{id}"
        return url

    def _do_request(self, method, data=None, id=None, timeout=None):
        url = self._build_request_url(id)
        params = dict(headers=self._build_request_headers())
        if timeout:
            params.update(dict(timeout=timeout))

        try:
            resp = self.session.request(method, url, json=data, **params)
        except RequestException as err:
            logger.error(
                "Error request for mailing message id %s: %s", id, err
            )
            return None

        if resp.ok:
            return resp.json()


class AbstractService(ABC):
    def __init__(self, client, resource_class, fields):
        self.client = client
        self.resource_class = resource_class
        self.fields = fields

    @abstractmethod
    def send(self, mailing):
        """Send data for mailing"""
