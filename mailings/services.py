from django.conf import settings
from django.utils import timezone

from .services_base import AbstractClient, AbstractService
from .models import Message, Mailing


class MailingService(AbstractService):
    id_field = "id"

    def __init__(self, client, resource_class, fields):
        if self.id_field not in fields:
            fields.append(self.id_field)
        super().__init__(client, resource_class, fields)

    def send(self, mailing=None):
        messages_data = (
            self._get_data(mailing) if mailing else self._get_all_data()
        )
        if messages_data is not None:
            sent_messages_ids = []
            for msg in messages_data:
                msg_id = msg[self.id_field]
                resp = self.client.execute(data=msg, id=msg_id)
                if resp:
                    sent_messages_ids.append(msg_id)
            self.resource_class.objects.filter(
                id__in=sent_messages_ids
            ).update(
                status=self.resource_class.SENT, date_status=timezone.now()
            )
            return len(sent_messages_ids)
        return 0

    def _get_data(self, mailing):
        now = timezone.now()
        messages = self.resource_class.objects.filter(
            mailing=mailing,
            status__in=[
                self.resource_class.DRAFT,
                self.resource_class.SENDING,
            ],
        )

        if now > mailing.date_end:
            messages.update(
                status=self.resource_class.OVERDUE, date_status=now
            )
            return

        if mailing.date_start < now < mailing.date_end:
            messages.update(
                status=self.resource_class.SENDING, date_status=now
            )

            return messages.with_mailing_and_code().values(*self.fields)

    def _get_all_data(self):
        now = timezone.now()

        overdue_messages = Mailing.objects.filter(date_end__lte=now)
        self.resource_class.objects.filter(
            mailing__in=overdue_messages
        ).update(status=self.resource_class.OVERDUE, date_status=now)

        mailings = Mailing.objects.exclude(date_start__gte=now).filter(
            date_end__gt=now
        )
        return (
            self.resource_class.objects.filter(
                mailing__in=mailings,
                status__in=[
                    self.resource_class.DRAFT,
                    self.resource_class.SENDING,
                ],
            )
            .with_mailing_and_code()
            .values(*self.fields)
        )


class MailingClient(AbstractClient):
    service_class = MailingService
    resource_class = Message
    fields = ["id", "client__phone", "mailing__text"]


api_client = MailingClient(
    url=f"{settings.PROBE_SERVER_URL.rstrip('/')}/v1/send",
    authorization=f"Bearer {settings.PROBE_SERVER_TOKEN}",
)
