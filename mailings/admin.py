from django.contrib import admin

from .models import Mailing, Message


class MailingAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "text",
        "date_start",
        "date_end",
        "get_tags",
        "get_codes",
    )

    @admin.display(description="tags")
    def get_tags(self, obj):
        return ", ".join([t.tag for t in obj.tags.all()])

    @admin.display(description="codes")
    def get_codes(self, obj):
        return ", ".join([str(c.code) for c in obj.operator_codes.all()])


class MessageAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "mailing",
        "get_text",
        "client",
        "status",
        "date_created",
        "date_status",
    )

    @admin.display(description="text")
    def get_text(self, obj):
        return obj.mailing.text


admin.site.register(Mailing, MailingAdmin)
admin.site.register(Message, MessageAdmin)
