from rest_framework import serializers

from clients.models import OperatorCode, Tag
from .models import Mailing, Message


class MailingSerializer(serializers.ModelSerializer):
    operator_codes = serializers.SlugRelatedField(
        slug_field="code",
        many=True,
        queryset=OperatorCode.objects.all(),
    )
    tags = serializers.SlugRelatedField(
        slug_field="tag",
        many=True,
        queryset=Tag.objects.all(),
    )

    class Meta:
        model = Mailing
        fields = fields = (
            "id",
            "date_start",
            "date_end",
            "text",
            "tags",
            "operator_codes",
        )
        read_only_fields = ("id",)

    def create(self, validated_data):
        operator_codes = validated_data.pop("operator_codes")
        tags = validated_data.pop("tags")

        mailing = self.Meta.model.objects.create(**validated_data)
        mailing.tags.add(*tags)
        mailing.operator_codes.add(*operator_codes)
        mailing.save()

        return mailing

    def update(self, instance, validated_data):
        operator_codes = validated_data.get("operator_codes")
        tags = validated_data.get("tags")

        if tags:
            instance.tags.set(tags)
        if operator_codes:
            instance.operator_codes.set(operator_codes)

        instance.date_start = validated_data.get(
            "date_start", instance.date_start
        )
        instance.date_end = validated_data.get("date_end", instance.date_end)
        instance.text = validated_data.get("text", instance.text)
        instance.save()
        return instance


class MailingStatiscticSerializer(serializers.ModelSerializer):
    clients = serializers.IntegerField()
    draft = serializers.IntegerField()
    sending = serializers.IntegerField()
    sent = serializers.IntegerField()
    overdue = serializers.IntegerField()
    delete = serializers.IntegerField()

    class Meta:
        model = Mailing
        fields = (
            "id",
            "date_start",
            "date_end",
            "text",
            "clients",
            "draft",
            "sending",
            "sent",
            "overdue",
            "delete",
        )


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = (
            "uuid",
            "date_created",
            "date_status",
            "status",
            "mailing",
            "client",
        )
        read_only_fields = fields
