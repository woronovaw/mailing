import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _

from auditlog.registry import auditlog

from clients.models import Client, OperatorCode, Tag


class MessageQuerySet(models.QuerySet):
    def with_mailing_and_code(self):
        return self.prefetch_related("mailing", "code")


class Message(models.Model):
    DRAFT = 0
    SENDING = 1
    SENT = 2
    OVERDUE = 3
    DELETED = 4
    STATUS_CHOICES = (
        (DRAFT, "Draft"),
        (SENDING, "Sending"),
        (SENT, "Sent"),
        (OVERDUE, "Overdue"),
        (DELETED, "Deleted"),
    )

    uuid = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)
    date_created = models.DateTimeField(_("created date"), auto_now_add=True)
    date_status = models.DateTimeField(_("date of change status"))
    status = models.IntegerField(
        _("status"), choices=STATUS_CHOICES, default=DRAFT
    )
    mailing = models.ForeignKey(
        "Mailing",
        verbose_name=_("mailing"),
        on_delete=models.CASCADE,
        related_name="messages",
    )
    client = models.ForeignKey(
        Client,
        verbose_name=_("client"),
        on_delete=models.SET_NULL,
        null=True,
        related_name="messages",
    )

    objects = MessageQuerySet.as_manager()

    def __str__(self):
        return str(self.id)


class Mailing(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    text = models.TextField(_("text"))
    tags = models.ManyToManyField(
        Tag, verbose_name=_("tags"), blank=True, related_name="mailings"
    )
    operator_codes = models.ManyToManyField(
        OperatorCode,
        verbose_name=_("codes"),
        blank=True,
        related_name="mailings",
    )
    date_start = models.DateTimeField(_("start date"))
    date_end = models.DateTimeField(_("end date"))

    def __str__(self):
        return str(self.id)


auditlog.register(Mailing)
