import logging

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import transaction
from django.utils import timezone

from clients.models import Client
from .models import Mailing, Message
from .services import api_client


logger = logging.getLogger(__name__)


def create_bulk_messages(mailing, clients):
    now = timezone.now()
    Message.objects.bulk_create(
        [
            Message(date_status=now, mailing=mailing, client=c)
            for c in clients.iterator()
        ]
    )


def on_transaction_commit(func):
    def inner(*args, **kwargs):
        transaction.on_commit(lambda: func(*args, **kwargs))

    return inner


@receiver(post_save, sender=Mailing, dispatch_uid="create_and_update_messages")
@on_transaction_commit
def create_messages(sender, instance, created, **kwargs):
    tags = instance.tags.all()
    operator_codes = instance.operator_codes.all()
    clients = Client.objects.all()

    if tags:
        clients = clients.filter(tag__in=tags)
    if operator_codes:
        clients = clients.filter(code__in=operator_codes)

    if created:
        create_bulk_messages(instance, clients)
    else:
        messages = Message.objects.filter(mailing=instance)
        messages.exclude(client__in=clients).update(status=Message.DELETED)

        new_clients = clients.exclude(id__in=[m.client.id for m in messages])
        create_bulk_messages(instance, new_clients)

    amount_messages = api_client.service.send(instance)
    logger.info("Sent %s messages", amount_messages)
