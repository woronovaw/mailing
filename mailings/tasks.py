from celery import shared_task
from celery.utils.log import get_task_logger


from .services import api_client


logger = get_task_logger(__name__)


@shared_task
def send_mailing():
    amount_messages = api_client.service.send()
    logger.info("Sent %s messages", amount_messages)
